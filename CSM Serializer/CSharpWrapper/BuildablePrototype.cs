﻿using System;
using System.Reflection;
using System.Collections.Generic;
using System.Collections.Concurrent;
using System.Threading.Tasks;

namespace CSM
{
    public class BuildablePrototype
    {
        private struct CallbackTypeBinding
        {
            internal Delegate InstatiateMethod;
            internal CSMInstanceFactory.InstantiateType CallbackType;

            internal CallbackTypeBinding(Delegate instantiatemethod, CSMInstanceFactory.InstantiateType type)
            {
                InstatiateMethod = instantiatemethod;
                CallbackType = type;
            }
        }

        private struct FieldBinding
        {

            internal MemberInfo Field { get; }
            internal CallbackTypeBinding Binding { get; }

            internal FieldBinding(in MemberInfo field, in CallbackTypeBinding binding)
            {
                Field = field;
                Binding = binding;
            }

        }

        private readonly FieldBinding[] m_ObjectFields;

        private readonly static ConcurrentDictionary<Type, BuildablePrototype> m_LoadedPrototypes = new ConcurrentDictionary<Type, BuildablePrototype>();

        private CallbackTypeBinding m_CreateCallback;

        private Type TargetType { get; }
        private TypeCode MyTypeCode { get; }

        private BuildablePrototype(ref Type targettype)
        {
            Delegate action = CSMInstanceFactory.CreateInstanceCallbackByType(targettype, out CSMInstanceFactory.InstantiateType callbacktype);

            m_CreateCallback = new CallbackTypeBinding(action, callbacktype);

            List<KeyValuePair<MemberInfo, CallbackTypeBinding>> fields = new List<KeyValuePair<MemberInfo, CallbackTypeBinding>>();

            FindFields(fields, targettype);

            m_ObjectFields = new FieldBinding[fields.Count];
            for (int i = 0; i < fields.Count; i++)
            {
                m_ObjectFields[i] = new FieldBinding(fields[i].Key, fields[i].Value);
            }

            TargetType = targettype;
            MyTypeCode = Type.GetTypeCode(TargetType);

            m_LoadedPrototypes.TryAdd(targettype, this);
        }

        public object[] BuildObjects(string[][] objvaluelines)
        {
            object[] outputobjects = new object[objvaluelines.Length];
            BuildablePrototype prototype = this;

            //async method to create objects
            //void CreateObjects(in int startindex, in int endindex)
            //{
            //    for (int i = startindex; i < endindex; i++)
            //    {
            //        outputobjects[i] = prototype.Build(objvaluelines[i]);
            //    }
            //}


            ////split workload accross different tasks
            //Task[] buildingtasks = new Task[3];
            //int offset = objvaluelines.Length / 3;
            //for (int i = 0; i < buildingtasks.Length; i++)
            //{
            //    int endindex = objvaluelines.Length - (offset * i);
            //    int beginindex = i * offset;
            //    endindex = endindex < offset ? endindex : offset;
            //    buildingtasks[i] = Task.Factory.StartNew(() => CreateObjects(beginindex, endindex));
            //}

            //Task.WaitAll(buildingtasks);

            for (int i = 0; i < objvaluelines.Length; i++)
            {
                object obj = prototype.Build(objvaluelines[i]);
                outputobjects[i] = obj;
            }

            return outputobjects;
        }

        private object Build(in string[] values)
        {
            object obj;

            if (m_ObjectFields.Length != 0)
            {
                obj = m_CreateCallback.InstatiateMethod.DynamicInvoke();

                int fieldinfoindex = 0;
                int valueturn = 0;

                while (fieldinfoindex != m_ObjectFields.Length)
                {
                    BuildObject(values, obj, ref fieldinfoindex, ref valueturn);
                }
            }
            else
            {
                obj = SetValue(MyTypeCode, values[0]);
            }

            return obj;
        }

        private void BuildObject(in string[] values,in object targetobject, ref int currentfieldinfoindex, ref int valueturn)
        {
            if (currentfieldinfoindex == m_ObjectFields.Length) return;

            MemberInfo member = m_ObjectFields[currentfieldinfoindex].Field;
            Type newtype = CSMObjFactoryShared.FieldType(m_ObjectFields[currentfieldinfoindex].Field);

            TypeCode objtypecode = Type.GetTypeCode(newtype);

            if (!CSMObjFactoryShared.IsPrimitiveType(objtypecode))
            {
                CallbackTypeBinding currentfield = m_ObjectFields[currentfieldinfoindex].Binding;

                object child = currentfield.InstatiateMethod.DynamicInvoke();
                currentfieldinfoindex++;

                if (currentfield.CallbackType == CSMInstanceFactory.InstantiateType.Derived)
                    child = Convert.ChangeType(child, newtype);

                BuildObject(values, child, ref currentfieldinfoindex, ref valueturn);
                return;
            }

            SetValueInMemberInfo(
                member,
                targetobject,
                SetValue(objtypecode, values[valueturn]));

            valueturn++;
            currentfieldinfoindex++;
        }

        private static void FindFields(in List<KeyValuePair<MemberInfo, CallbackTypeBinding>> fields, in Type currenttype)
        {
            if (CSMObjFactoryShared.IsPrimitiveType(Type.GetTypeCode(currenttype))) return;

            MemberInfo[] typefields = CSMObjFactoryShared.GetObjMembers(currenttype);

            for (int i = 0; i < typefields.Length; i++)
            {
                Type fieldtypetype = CSMObjFactoryShared.FieldType(typefields[i]);
                Delegate createdelegate =
                    CSMInstanceFactory.CreateInstanceCallbackByType(fieldtypetype, out CSMInstanceFactory.InstantiateType fieldinstantiatetype);
                fields.Add(
                    new KeyValuePair<MemberInfo, CallbackTypeBinding>(
                        typefields[i],
                        new CallbackTypeBinding(createdelegate, fieldinstantiatetype)));

                FindFields(fields, fieldtypetype);
            }
        }

        private static void SetValueInMemberInfo(in MemberInfo member, in object targetobj, in object value)
        {
            if (member is FieldInfo field)
                field.SetValue(targetobj,value);
            else if (member is PropertyInfo property)
                property.SetValue(targetobj, value);
        }

        private static object SetValue(in TypeCode fieldtype, in string value)
        {
            if (value == "" && fieldtype != TypeCode.String)
            {
                return null;
            }
            switch (fieldtype)
            {
                case TypeCode.String:
                    return value;
                case TypeCode.Boolean:
                    return bool.Parse(value);
                case TypeCode.Byte:
                    return byte.Parse(value);
                case TypeCode.Char:
                    return char.Parse(value);
                case TypeCode.Int32:
                    return int.Parse(value);
                case TypeCode.Int16:
                    return short.Parse(value);
                case TypeCode.Double:
                    return double.Parse(value);
                case TypeCode.Single:
                    return float.Parse(value);
                case TypeCode.Int64:
                    return long.Parse(value);
                case TypeCode.Decimal:
                    return decimal.Parse(value);
                case TypeCode.SByte:
                    return sbyte.Parse(value);
                case TypeCode.UInt32:
                    return uint.Parse(value);
                case TypeCode.UInt16:
                    return ushort.Parse(value);
                case TypeCode.UInt64:
                    return ulong.Parse(value);
                default:
                    return null;
            }
        }

        public static BuildablePrototype GetOrCreate(ref Type targettype)
        {
            if (m_LoadedPrototypes.ContainsKey(targettype))
            {
                m_LoadedPrototypes.TryGetValue(targettype, out BuildablePrototype proto);
                return proto;
            }

            return new BuildablePrototype(ref targettype);
        }
    }
}