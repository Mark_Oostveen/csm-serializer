﻿using System;
using System.Linq;
using System.Reflection;

namespace CSM
{
    internal static class CSMObjFactoryShared
    {
        internal static MemberInfo[] GetObjMembers(in Type objtype)
        {
            return objtype.GetMembers(BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance).Where(
                x =>
                {
                    MemberTypes type = x.MemberType;
                    if (type == MemberTypes.Field || type == MemberTypes.Event || type == MemberTypes.Property)
                        return true;

                    return false;
                }).ToArray();
        }

        internal static Type FieldType(MemberInfo member)
        {
            MemberTypes type = member.MemberType;

            switch (type)
            {
                case MemberTypes.Field:
                    FieldInfo field = member as FieldInfo;
                    return field.FieldType;

                case MemberTypes.Property:
                    PropertyInfo property = member as PropertyInfo;
                    return property.PropertyType;

                case MemberTypes.Event:
                    EventInfo eventinfo = member as EventInfo;
                    return eventinfo.EventHandlerType;


                default: return null;
            }

        }

        internal static bool IsPrimitiveType(in TypeCode checkingtype)
        {
            switch (checkingtype)
            {
                case TypeCode.Object:
                    return false;

                default:
                    return true;
            }
        }
    }
}
