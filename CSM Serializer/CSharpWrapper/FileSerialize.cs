﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Reflection;
using System.Collections.Concurrent;

namespace CSM
{
    public static class FileSerialize
    {
        private struct SerializeTargetInfo
        {
            internal readonly TypeMember[] Members;
            internal readonly int FieldCount;

            internal SerializeTargetInfo(in TypeMember[] members, in int fieldcount)
            {
                Members = members;
                FieldCount = fieldcount;
            }
        }

        private class TypeMember
        {
            internal readonly MemberInfo MyInfo;
            internal TypeMemberField[] Fields;
            internal readonly TypePrototype ProtoType;
            internal TypeMember(in MemberInfo myinfo, TypePrototype prototype)
            {
                MyInfo = myinfo;
                ProtoType = prototype;
            }
        }

        private struct TypeMemberField
        {
            internal TypeMember Member;
            internal TypePrototype Prototype;

            public TypeMemberField(in TypeMember member, in TypePrototype prototype)
            {
                Member = member;
                Prototype = prototype;
            }
        }

        private struct TypePrototype
        {
            internal readonly Type ThisType;
            internal readonly TypePrototype[] Members;

            internal TypePrototype(in Type thistype, in TypePrototype[] members)
            {
                ThisType = thistype;
                Members = members;
            }
        }

        private const char SEPARATOR = ',';
        private readonly static ConcurrentDictionary<Type, SerializeTargetInfo> m_ReviewedTypes;
        private readonly static ConcurrentDictionary<Type, TypePrototype> m_ProcessedPrototypes;



        private class FileSerializerObj
        {
            internal Thread m_Worker;
            internal ConcurrentDictionary<Type, List<object[]>> m_ObjectValuesPerType;
        }

        static FileSerialize()
        {
            m_ReviewedTypes = new ConcurrentDictionary<Type, SerializeTargetInfo>(2,1000);
            m_ProcessedPrototypes = new ConcurrentDictionary<Type, TypePrototype>(2, 1000);
        }

        private static string[][][] CreateCSMFile(in object[] objects)
        {
            FileSerializerObj serializer = new FileSerializerObj();

            TypePrototype[] sortedprototypes = ProcessRaw(objects, serializer);


            string[][][] output = Serialize(sortedprototypes, serializer);

            return output;
        }

        public static string[][][] SerializeObjects(ref object[] objectpointers)
        {
            return CreateCSMFile(objectpointers.Where(x => x != null).ToArray());
        }

        /// <summary>
        /// Process Raw
        /// </summary>
        /// <param name="objects"></param>
        /// <returns>sorted list of prototypes</returns>
        private static TypePrototype[] ProcessRaw(object[] objects, FileSerializerObj serializerdata)
        {
            //Process raw types
            Type[] targettypes = ExtractTypes(objects);
            Type[] uniquetypes = targettypes.Distinct().ToArray();
            ProcessRawTypes(uniquetypes);

            //create new dictionary for each type to store object values, and fill with all values
            serializerdata.m_Worker = new Thread(() =>
            {
                FillDictionary(targettypes, uniquetypes, serializerdata);
                ProcessObjectValues(objects, targettypes, serializerdata);
            });
            serializerdata.m_Worker.Start();

            //Sort prototypes on main
            TypePrototype[] sortedtypes = m_ProcessedPrototypes.Values.ToArray();
            SortPrototypes(ref sortedtypes);

            return sortedtypes;
        }

        private static string[][][] Serialize(in TypePrototype[] sortedprototypes,in FileSerializerObj serializerdata)
        {
            //Process assemblies and sort prototypes
            string[][] assemblies = ProcessAssemblies(m_ProcessedPrototypes.Keys.GetEnumerator(), out IDictionary<Type, int> typecollection);
            string[][] prototypes = ProcessPrototypes(sortedprototypes, typecollection);

            //Wait for value processing thread to finish
            serializerdata.m_Worker.Join();

            //convert valuelists to stringblocks
            IEnumerator<KeyValuePair<Type, List<object[]>>> collection = serializerdata.m_ObjectValuesPerType.GetEnumerator();
            string[][] blocks = ProcessSerializedBlocks(sortedprototypes, collection, serializerdata);
            collection.Dispose();

            return new[]{ assemblies , prototypes , blocks };
        }

        #region Processing RawObjects Types

        private static Type[] ExtractTypes(in object[] objects)
        {
            Type[] output = new Type[objects.Length];
            for (int i = 0; i < objects.Length; i++)
            {
                output[i] = objects[i].GetType();
            }

            return output;
        }

        private static void ProcessRawTypes(in Type[] uniquetypes)
        {
            //remove duplicates
            for (int i = 0; i < uniquetypes.Length; i++)
            {
                TypeToPrototype(uniquetypes[i]);
                ProcessRawType(uniquetypes[i]);
            }
        }

        private static void ProcessRawType(in Type target)
        {
            if (!m_ReviewedTypes.ContainsKey(target))
            {
                MemberInfo[] members = CSMObjFactoryShared.GetObjMembers(target);

                TypeMember[] typemembers = MembersToTypeMember(members);

                int fieldcount = 0;
                for (int i = 0; i < typemembers.Length; i++)
                {
                    FindTotalFieldCount(ref fieldcount, typemembers[i]);
                }

                m_ReviewedTypes.TryAdd(target, new SerializeTargetInfo(typemembers, fieldcount));
            }

        }

        private static void FindTotalFieldCount(ref int currentcount, TypeMember prototype)
        {
            bool IsPrimitive(in Type target) =>
                CSMObjFactoryShared.IsPrimitiveType(Type.GetTypeCode(target));


            if (IsPrimitive(prototype.ProtoType.ThisType))
            {
                currentcount++;
                return;
            }

            Queue <KeyValuePair<int, TypeMember>> derived = new Queue<KeyValuePair<int, TypeMember>>(15);
            int startingindex = 0;
            TypeMember currentmember = prototype;
            while (true)
            {
                for (int i = startingindex; i < currentmember.Fields.Length; i++)
                {
                    TypeMember next = prototype.Fields[i].Member;
                    if (next == null)
                    {
                        if (derived.Count > 0)
                        {
                            KeyValuePair<int, TypeMember> pair = derived.Dequeue();
                            currentmember = pair.Value;
                            startingindex = pair.Key;
                            break;
                        }
                        Console.WriteLine("ERROR TypeMember not defined");
                        return;
                    }

                    if (IsPrimitive(next.ProtoType.ThisType))
                    {
                        currentcount++;
                        continue;
                    }
                    derived.Enqueue(new KeyValuePair<int, TypeMember>(i, next));
                    currentmember = next;
                }
            }
        }

        private static TypePrototype TypeToPrototype(in Type target)
        {
            if (!m_ProcessedPrototypes.ContainsKey(target))
            {
                bool isprimitive = CSMObjFactoryShared.IsPrimitiveType(Type.GetTypeCode(target));
                if (isprimitive)
                    return new TypePrototype(target, new TypePrototype[0]);

                MemberInfo[] members = CSMObjFactoryShared.GetObjMembers(target);
                TypePrototype[] prototypes = new TypePrototype[members.Length];
                for (int i = 0; i < members.Length; i++)
                {
                    Type fieldtype = GetFieldTypeOfType(members[i]);
                    if (fieldtype != null)
                    {
                        if (!m_ProcessedPrototypes.ContainsKey(fieldtype))
                        {
                            prototypes[i] = TypeToPrototype(fieldtype);
                            m_ProcessedPrototypes.TryAdd(fieldtype, prototypes[i]);
                        }
                        else
                        {
                            m_ProcessedPrototypes.TryGetValue(fieldtype, out prototypes[i]);
                        }
                    }
                }
                TypePrototype dictionaryprototype = new TypePrototype(target, prototypes);
                m_ProcessedPrototypes.TryAdd(target, dictionaryprototype);
            }

            m_ProcessedPrototypes.TryGetValue(target, out TypePrototype prototype);
            return prototype;
        }

        private static Type GetFieldTypeOfType(in MemberInfo member)
        {
            if (member is FieldInfo field)
                return field.FieldType;

            if (member is PropertyInfo property)
                return property.PropertyType;

            return null;
        }

        private static TypeMember[] MembersToTypeMember(in MemberInfo[] members)
        {
            TypeMember[] typemembers = new TypeMember[members.Length];

            for (int i = 0; i < typemembers.Length; i++)
            {
                typemembers[i] = MemberToTypeMember(members[i]);
            }

            return typemembers;
        }

        private static TypeMember MemberToTypeMember(in MemberInfo member)
        {
            Type fieldtype = GetFieldTypeOfType(member);
            if (fieldtype == null) return null;


            TypePrototype myprototype = FileSerialize.TypeToPrototype(fieldtype);
            MemberInfo[] variablemembers = CSMObjFactoryShared.GetObjMembers(fieldtype);
            TypeMemberField[] fields;

            if (!CSMObjFactoryShared.IsPrimitiveType(Type.GetTypeCode(fieldtype)))
            {
                fields = new TypeMemberField[variablemembers.Length];
                for (int j = 0; j < fields.Length; j++)
                {
                    Type variablememberfieldtype = GetFieldTypeOfType(variablemembers[j]);
                    fields[j] = TypeToTypeMemberField(variablememberfieldtype, fieldtype, variablemembers[j]);
                }
            }
            else
                fields = new TypeMemberField[1]{ TypeToTypeMemberField(GetFieldTypeOfType(variablemembers[0]), fieldtype, variablemembers[0]) };

            TypeMember outputmember = new TypeMember(member, myprototype)
            {
                Fields = fields
            };
            return outputmember;

        }

        private static TypeMemberField TypeToTypeMemberField(Type field, Type previoustarget, MemberInfo member)
        {
            if (field == null) return new TypeMemberField();
            if (CSMObjFactoryShared.IsPrimitiveType(Type.GetTypeCode(field)) &&
                CSMObjFactoryShared.IsPrimitiveType(Type.GetTypeCode(previoustarget))) return new TypeMemberField();
            TypePrototype variableprototype = TypeToPrototype(field);
            TypeMember newmember = MemberToTypeMember(member);
            return new TypeMemberField(newmember, variableprototype);
        }

        #endregion
        #region Processing RawObject Values

        private static void FillDictionary(in Type[] types, Type[] uniquetypes, in FileSerializerObj serializer)
        {
            //create new dictionary for each type to store objectvalues
            serializer.m_ObjectValuesPerType = new ConcurrentDictionary<Type, List<object[]>>(2, types.Length);
            for (int i = 0; i < uniquetypes.Length; i++)
            {
                serializer.m_ObjectValuesPerType.TryAdd(uniquetypes[i], new List<object[]>());
            }
        }

        private static void ProcessObjectValues(in object[] objects, in Type[] types, in FileSerializerObj serializer)
        {
            for (int i = 0; i < objects.Length && i < types.Length; i++)
            {
                //Get member information, and the corrosponding list containing all object values for this type
                if (!m_ReviewedTypes.TryGetValue(types[i], out SerializeTargetInfo currentinfo)) continue;
                if (!serializer.m_ObjectValuesPerType.TryGetValue(types[i], out List<object[]> typeobjlist)) continue;
                object[] objvaluearr = new object[currentinfo.FieldCount];

                //lock list for adding since list is not threadsafe
                lock (typeobjlist)
                {
                    typeobjlist.Add(objvaluearr);
                }

                //process values
                TypeMember[] fields = currentinfo.Members;
                int currentindex = 0;
                for (int j = 0; j < fields.Length; j++)
                {
                    ProcessRawValues(objects[i], fields[j], ref objvaluearr, ref currentindex);
                }
            }
        }

        private static void ProcessRawValues(in object target, in TypeMember info, ref object[] valuelist, ref int currentindex)
        {
            object value = target == null ? null : GetValueFromMemberInfo(target, info.MyInfo);
            if (CSMObjFactoryShared.IsPrimitiveType(Type.GetTypeCode(info.ProtoType.ThisType)))
            {
                value = target == null ? null : GetValueFromMemberInfo(target, info.MyInfo);

                valuelist[currentindex] = value;
                currentindex++;
            }

            for (int i = 0; i < info.Fields.Length; i++)
            {
                if (info.Fields[i].Member == null) continue;
                ProcessRawValues(value, info.Fields[i].Member, ref valuelist, ref currentindex);
            }
        }

        private static object GetValueFromMemberInfo(in object target ,in MemberInfo info)
        {
            if (info is FieldInfo myfieldinfo)
                return myfieldinfo.GetValue(target);

            if (info is PropertyInfo mypropertyinfo)
                return mypropertyinfo.GetValue(target);

            return null;
        }
        #endregion
        #region Processing Assemblies

        private static string[][] ProcessAssemblies(in IEnumerator<Type> types, out IDictionary<Type, int> typecollection)
        {
            List<Module> modules = new List<Module>(100);
            typecollection = new Dictionary<Type, int>();

            while (types.MoveNext())
            {
                Module newmodule = GetAssembly(types.Current);
                if (!modules.Contains(newmodule))
                    modules.Add(newmodule);
                typecollection.Add(types.Current, modules.Count - 1);
            }

            string[][] output = new string[modules.Count][];
            for (int i = 0; i < output.Length; i++)
            {
                output[i] = new[] { "C#", modules[i].Name };
            }

            return output;
        }

        private static Module GetAssembly(Type input)
        {
            return input.Module;
        }

        #endregion
        #region Processing Prototypes

        /// <summary>
        /// Sort the input array based on members for each prototype
        /// </summary>
        /// <param name="types"></param>
        private static void SortPrototypes(ref TypePrototype[] types)
        {
            bool containsmistake = true;
            while (containsmistake)
            {
                containsmistake = false;
                for (int i = 0; i < types.Length; i++)
                {
                    TypePrototype current = types[i];
                    for (int j = 0; j < current.Members.Length; j++)
                    {
                        int elementindex = Array.IndexOf(types, current.Members[j]);
                        if (elementindex > i)
                        {
                            containsmistake = true;
                            TypePrototype old = types[i];
                            types[i] = types[elementindex];
                            types[elementindex] = old;
                        }
                    }
                }
            }
        }

        private static string[][] ProcessPrototypes(in TypePrototype[] types, in IDictionary<Type, int> typecollection)
        {
            string[][] prototypes = new string[types.Length][];
            for (int i = 0; i < prototypes.Length; i++)
            {
                int[] memberindexes = new int[types[i].Members.Length];
                for (int j = 0; j < memberindexes.Length; j++)
                {
                    memberindexes[j] = Array.IndexOf(types, types[i].Members[j]);
                }

                prototypes[i] = CreatePrototypeText(types[i].ThisType, memberindexes, i, typecollection);
            }

            return prototypes;
        }



        private static string[] CreatePrototypeText(in Type targettype, in int[] memberprototypeindexes, in int index, in IDictionary<Type, int> typecollection)
        {
            string[] output = new string[4 + memberprototypeindexes.Length];
            output[0] = index.ToString();
            output[1] = !typecollection.TryGetValue(targettype, out int value) ? "" : value.ToString();
            output[2] = targettype.FullName;
            output[3] = "C#";

            for (int i = 0; i < memberprototypeindexes.Length; i++)
            {
                output[4 + i] = memberprototypeindexes[i].ToString();
            }

            return output;
        }

        #endregion
        #region Processing SerializedBlocks

        private static string[][] ProcessSerializedBlocks(in TypePrototype[] prototypes, in IEnumerator<KeyValuePair<Type, List<object[]>>> collection, in FileSerializerObj serializer)
        {
            List<string[]> blocks = new List<string[]>(serializer.m_ObjectValuesPerType.Count);
            int currentindex = 0;
            while (collection.MoveNext())
            {
                List<object[]> toserializeobjectlist = collection.Current.Value;
                m_ProcessedPrototypes.TryGetValue(collection.Current.Key, out TypePrototype thisprototype);
                List<string> newblockprototype = CreateBlock(currentindex, Array.IndexOf(prototypes, thisprototype), toserializeobjectlist.Count);

                for (int i = 0; i < toserializeobjectlist.Count; i++)
                {
                    newblockprototype.Add(ToBlockString(toserializeobjectlist[i]));
                }

                blocks.Add(newblockprototype.ToArray());
                currentindex++;
            }


            return blocks.ToArray();
        }
        private static List<string> CreateBlock(in int index, in int prototypeid, in int size)
        {
            List<string> prototype = new List<string>(3) { index.ToString(), size.ToString(), prototypeid.ToString()};
            //Index to is the length of items included in this block
            //index 2 is the prototype index and this needs to be set at a later time

            return prototype;
        }

        private static string ToBlockString(in object[] values)
        {
            StringBuilder builder = new StringBuilder();
            for (int i = 0; i < values.Length; i++)
            {
                builder.Append(values[i]);

                if (i + 1 < values.Length)
                    builder.Append(SEPARATOR);
            }

            return builder.ToString();
        }

        #endregion
    }
}