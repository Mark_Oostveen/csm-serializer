﻿using System;
using System.Reflection;
using System.Reflection.Emit;
using System.Collections.Generic;

namespace CSM
{
    /// <summary>
    ///     Utility class for rewinding functions
    /// </summary>
    public static class CSMInstanceFactory
    {
        public enum InstantiateType
        {
            Activator,
            CIL,
            Derived,
            None
        }

        private class InstancePrefab
        {
            internal Delegate Method { get; }
            internal InstantiateType Type { get; }

            public InstancePrefab(in Delegate method, in InstantiateType type)
            {
                Method = method;
                Type = type;
            }
        }

        private readonly static MethodInfo NonGenericInstantiateMethod;
        private readonly static ModuleBuilder DynamicModule;

        //cached instatiatemethods
        private readonly static Dictionary<Type, InstancePrefab> m_DynamicTypeConstructors;

        static CSMInstanceFactory()
        {
            //Field initializers
            m_DynamicTypeConstructors = new Dictionary<Type, InstancePrefab>();

            //cache the generic method
            NonGenericInstantiateMethod = typeof(CSMInstanceFactory).GetMethod("CreateObject");

            AssemblyBuilder assemblybuilder =
                AppDomain.CurrentDomain.DefineDynamicAssembly(new AssemblyName("CSMDynamic"),
                    AssemblyBuilderAccess.Run);
            DynamicModule = assemblybuilder.DefineDynamicModule("CSM");
        }

        private delegate T CreatePrototypeCallback<T>();

        public static T CreateObject<T>()
        {
            return Activator.CreateInstance<T>();
        }

        internal static Delegate CreateInstanceCallbackByType(in Type type, out InstantiateType instatiattype)
        {
            if (type != null)
            {
                //lock access to the dictionary
                lock (m_DynamicTypeConstructors)
                {
                    //search for existing and return
                    m_DynamicTypeConstructors.TryGetValue(type, out InstancePrefab info);
                    if (info != null)
                    {
                        instatiattype = info.Type;
                        return info.Method;
                    }

                    //check constructor
                    ConstructorInfo ctor = null;

                    ConstructorInfo[] ctors =
                        type.GetConstructors(BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance);
                    for (int i = ctors.Length - 1; i >= 0; i--)
                    {
                        if (ctors[i].GetParameters().Length != 0) continue;

                        ctor = ctors[i];
                        break;
                    }


                    if (ctor != null)
                    {
                        //try and create an deriving class that can be instantiated instead of the origional type to gain performance
                        if (type.IsClass && !type.IsSealed && (ctor.IsFamilyOrAssembly || ctor.IsFamily))
                        {
                            instatiattype = InstantiateType.Derived;
                            Delegate derivedmethod = DefineDynamicType(type, ctor);
                            m_DynamicTypeConstructors.Add(type, new InstancePrefab(derivedmethod, InstantiateType.Derived));
                            return derivedmethod;
                        }
                    }
                    else
                    {
                        //construct new objects using the activator class 'which is the slowest way'
                        Type callbackoutputtype = typeof(CreatePrototypeCallback<>);
                        callbackoutputtype = callbackoutputtype.MakeGenericType(type);

                        MethodInfo createdelegateinfo = NonGenericInstantiateMethod.MakeGenericMethod(type);


                        instatiattype = InstantiateType.Activator;
                        Delegate activatormethod = Delegate.CreateDelegate(callbackoutputtype, createdelegateinfo);
                        m_DynamicTypeConstructors.Add(type, new InstancePrefab(activatormethod, InstantiateType.Activator));
                        return activatormethod;
                    }

                    instatiattype = InstantiateType.CIL;
                    Delegate cilmethod = CilCreateInstanceByType(type, ctor);
                    m_DynamicTypeConstructors.Add(type, new InstancePrefab(cilmethod, InstantiateType.CIL));
                    return cilmethod;
                }
            }

            instatiattype = InstantiateType.None;
            return null;
        }

        /// <summary>
        /// high performance object creator, only suitable for types with a userimplemented constructor without any parameters
        /// </summary>
        /// <param name="type"></param>
        /// <param name="ctor"></param>
        /// <returns></returns>
        private static Delegate CilCreateInstanceByType(in Type type, in ConstructorInfo ctor)
        {
            DynamicMethod method = new DynamicMethod("Create" + type.Name, type, Type.EmptyTypes, DynamicModule);
            ILGenerator generator = method.GetILGenerator(4);

            generator.Emit(OpCodes.Newobj, ctor);
            generator.Emit(OpCodes.Ret);

            Type delegatetype = typeof(CreatePrototypeCallback<>);
            delegatetype = delegatetype.MakeGenericType(type);

            return method.CreateDelegate(delegatetype);
        }


        /// <summary>
        /// define a dynamic type which inhearits from thes target type defined in a dynamic assembly
        /// </summary>
        /// <param name="targettype"></param>
        /// <returns></returns>
        private static Delegate DefineDynamicType(in Type targettype, in ConstructorInfo targetconstructor)
        {
            TypeBuilder typebuiler = DynamicModule.DefineType("CSM_DYNAMIC_" + targettype.FullName);
            typebuiler.SetParent(targettype);
            ConstructorBuilder ctorbuilder = typebuiler.DefineConstructor(MethodAttributes.Public, CallingConventions.Standard, Type.EmptyTypes);

            ILGenerator gen = ctorbuilder.GetILGenerator();

            gen.Emit(OpCodes.Ldarg_0);
            gen.Emit(OpCodes.Call, targetconstructor);
            gen.Emit(OpCodes.Ret);

            typebuiler.CreateType();

            ConstructorInfo constructor = typebuiler.GetConstructor(BindingFlags.Public | BindingFlags.Instance, null, Type.EmptyTypes, new ParameterModifier[0]);

            Delegate output = new Func<object>(() => constructor.Invoke(null));

            return output;
        }
    }
}