#pragma once
#include "FileCSM.h"
#include "FilePrototype.h"

namespace CSM
{
	public ref class CSMManager
	{
	public:
		static void InitializeCsharpCSM();
		static void DisposeCsharpCSM();
		static FileCSM^ OpenFileAtLocation(System::String^% location, bool ignoreassemblies, bool ignoreprototypes, bool ignoreserializedblocks);
		static FileCSM^ OpenFile(System::String^% filecontents, bool ignoreassemblies, bool ignoreprototypes, bool ignoreserializedblocks);
		static FilePrototype^ CreateFileTemplate();
	};
}
