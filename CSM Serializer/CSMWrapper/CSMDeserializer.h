#pragma once

#include "..\CSMCORE\Header Files\CSMSerializedBlock.h"
#include "..\CSMCORE\Header Files\CSMPrototypeInfo.h"
#include "..\CSMCORE\Header Files\CSMDeserializerCore.h"

using namespace System;
using namespace System::Runtime::InteropServices;

namespace CSM
{
	public ref struct CSMDeserializer
	{
	public:
		delegate void* csharpbuilddelegate(CSMDeserializerCore&);

		static void StaticConstructor();

		static void StaticDestructor();

		CSMDeserializer(CSMDeserializerCore* baseDeserializer);
		~CSMDeserializer();
		array<System::Object^>^ Build();

	private:
		static void* Build(CSMDeserializerCore& deserializerobj);
		static GCHandle m_CallbackHandle;

		CSMDeserializerCore * m_CoreInstance;
	};
}
