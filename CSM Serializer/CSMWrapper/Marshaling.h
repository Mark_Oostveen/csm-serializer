#pragma once
#include <string>

namespace CSM
{

	ref class CSharpMarshalling
	{
	public:
		static std::string ToStdString(System::String^% input);
		static System::String^ ToSystemString(std::string& input);
	};
}
