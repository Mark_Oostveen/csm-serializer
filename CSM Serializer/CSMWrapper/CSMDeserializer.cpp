#include "CSMDeserializer.h"
#include "..\CSMCORE\Header Files\CSMSerializedBlock.h"
#include "CSharpManager.h"
#include "Marshaling.h"
#include "..\CSMCORE\Header Files\CSMDeserializerCore.h"
#include <cliext/vector>
#include <vector>

using namespace System::Collections::Generic;
using namespace System::Runtime::InteropServices;
using namespace System::Threading::Tasks;

namespace CSM
{
	void CSMDeserializer::StaticConstructor()
	{
		csharpbuilddelegate^ del = gcnew csharpbuilddelegate(Build);
		m_CallbackHandle = GCHandle::Alloc(del);
		CSMDeserializerCore::CSharpBuildDelegate dell = (CSMDeserializerCore::CSharpBuildDelegate) Marshal::GetFunctionPointerForDelegate(del).ToPointer();
		CSMDeserializerCore::CsharpCallback = dell;
	}

	void CSMDeserializer::StaticDestructor()
	{
		m_CallbackHandle.Free();
	}

	CSMDeserializer::~CSMDeserializer()
	{
		delete m_CoreInstance;
	}

	CSMDeserializer::CSMDeserializer(CSMDeserializerCore* baseDeserializer)
	{
		m_CoreInstance = baseDeserializer;
	}

	array<System::Object^>^ CSMDeserializer::Build()
	{
		GCHandle handle = GCHandle::FromIntPtr(System::IntPtr(m_CoreInstance->Build()));
		array<System::Object^>^ arr = static_cast<array<System::Object^>^>(handle.Target);
		handle.Free();
		return arr;
	}

	void* CSMDeserializer::Build(CSMDeserializerCore& deserializerobj)
	{
		CSMPrototypeInfo* info = deserializerobj.GetPrototypePTR();
		String^ systemtypename(CSharpMarshalling::ToSystemString(info->TypeName));
		System::String^ assemblyname = CSharpMarshalling::ToSystemString(*deserializerobj.Assembly);
		Type^ type = CSharpManager::FindType(systemtypename, assemblyname);
		BuildablePrototype^ proto = (BuildablePrototype^)BuildablePrototype::GetOrCreate(type);

		CSMSerializedBlock& block = *deserializerobj.GetBlockPTR();
		array<array<String^>^>^ blockvalues = gcnew array<array<String^>^>(block.DataSize);

		for (std::int32_t i = 0; i < block.DataSize; i++)
		{
			String^ blockcontentline = CSharpMarshalling::ToSystemString(block.Data[i]);
			blockvalues[i] = blockcontentline->Split(',');
		}

		GCHandle rethandle = GCHandle::Alloc(proto->BuildObjects(blockvalues));
		IntPtr^ ptr = GCHandle::ToIntPtr(rethandle);

		return ptr->ToPointer();
	}
}