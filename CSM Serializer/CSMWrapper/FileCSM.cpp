#include "FileCSM.h"
#include <iostream>
#include <string>
#include <sstream>
#include <vector>
#include <map>
#include "Marshaling.h"
#include "CSharpManager.h"
#include "..\CSMCORE\Header Files\FileUtility.h"
#include "..\CSMCORE\Header Files\CSMSerializedBlock.h"
#include "..\CSMCORE\Header Files\CSMFileCore.h"

namespace CSM
{
	FileCSM::FileCSM(std::stringstream& stream, bool ignoreassemblies, bool ignoreprototypes, bool ignoreserializedblocks)
	{
		CsharpAddPrototypeDelegate^ del = gcnew CsharpAddPrototypeDelegate(static_cast<FileCSM^>(this), &FileCSM::LoadCsharpType);
		m_AddprototypeCallbackHandle = GCHandle::Alloc(del);
		CSMFileCore::CSharpCreatePrototype dell = (CSMFileCore::CSharpCreatePrototype) Marshal::GetFunctionPointerForDelegate(del).ToPointer();
		m_BaseFile = new CSMFileCore(stream, ignoreassemblies, ignoreprototypes, ignoreserializedblocks, dell);
		m_DotNetLoadedTypes = gcnew ConcurrentDictionary<String^, BuildablePrototype^>();
		
	}

	FileCSM::~FileCSM()
	{
		delete m_BaseFile;
		m_AddprototypeCallbackHandle.Free();
	}

	System::Int32 FileCSM::GetBlockCount()
	{
		return m_BaseFile->GetSerializedBlockCount();
	}

	void FileCSM::LoadAssemblies()
	{
		std::stringstream assemblies(m_BaseFile->GetAssemblies());

		std::string line;
		while (std::getline(assemblies, line, '\n'))
		{
			size_t position = line.find(',');
			std::string language = line.substr(0, position);
			if (language == "C#")
			{
				std::string name = line.substr(position + 1, line.size());
				CSharpManager::TryLoadAssembly(name);
				m_BaseFile->LoadedAssemblies->push_back(name);
			}
		}
	}

	void FileCSM::MergeAssemblies(String ^% newblock)
	{
		m_BaseFile->GetAssemblies().clear();
		m_BaseFile->GetAssemblies().append(CSharpMarshalling::ToStdString(newblock));
		LoadAssemblies();
	}

	void FileCSM::LoadProtoTypes()
	{
		m_BaseFile->LoadProtoTypes();
	}

	void FileCSM::MergePrototype(String ^% newprototype)
	{
		m_BaseFile->MergePrototype(CSharpMarshalling::ToStdString(newprototype));
	}

	void FileCSM::PrepareSerializedBlocks()
	{
		m_BaseFile->PrepareSerializedBlocks();
	}

	array<CSMDeserializer^>^ FileCSM::LoadWholeFile()
	{
		LoadAssemblies();
		LoadProtoTypes();
		PrepareSerializedBlocks();
		array<CSMDeserializer^>^ outputarr = gcnew array<CSMDeserializer^>(m_BaseFile->GetSerializedBlockCount());
		for (int i = 0; i < outputarr->Length; i++)
		{
			outputarr[i] = GetDeserializer(i);
		}
		return outputarr;
	}

	void FileCSM::DisposeDeserializerArray(array<CSMDeserializer^>^% deserializers)
	{
		for (int i = 0; i < deserializers->Length; i++)
		{
			delete deserializers[i];
		}
	}

	bool FileCSM::LoadCsharpType(CSMPrototypeInfo& prototype)
	{
		//TODO implement loading of type of C# subsystem
		String^ name = CSharpMarshalling::ToSystemString(prototype.TypeName);
		String^ assemblyname = CSharpMarshalling::ToSystemString(m_BaseFile->LoadedAssemblies->at(prototype.m_AssemblyID));
		Type^ variabletype = CSharpManager::FindType(name, assemblyname);

		if (variabletype != nullptr)
		{
			if (m_DotNetLoadedTypes->ContainsKey(name)) return true;

			BuildablePrototype^ prototypestruct = BuildablePrototype::GetOrCreate(variabletype);
			m_DotNetLoadedTypes->TryAdd(name, prototypestruct);
			return true;
		}

		return false;
	}

	CSMDeserializer^ FileCSM::GetDeserializer(Int32 serializedblockindex)
	{
		CSMDeserializerCore* basedeserializer = m_BaseFile->GetDeserializer(serializedblockindex, m_BaseFile->LoadedAssemblies->at(serializedblockindex));
		return gcnew CSMDeserializer(basedeserializer);
	}
}