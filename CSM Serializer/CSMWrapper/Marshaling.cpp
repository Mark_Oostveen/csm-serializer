#pragma once
#include "Marshaling.h"
#include <msclr/marshal_cppstd.h>


namespace CSM
{
	using namespace System;
	using namespace System::Runtime::InteropServices;
	using namespace msclr::interop;

	std::string CSharpMarshalling::ToStdString(System::String^% input)
	{
		IntPtr ptr = Marshal::StringToCoTaskMemAnsi(input);
		char* stringcontents = static_cast<char*>(ptr.ToPointer());
		std::string output(stringcontents);
		Marshal::FreeHGlobal(ptr);
		return output;
	}

	String^ CSharpMarshalling::ToSystemString(std::string& input)
	{
		String^ managedstring = Marshal::PtrToStringAnsi(IntPtr(const_cast<char*>(input.c_str())));
		return managedstring;
	}
}
