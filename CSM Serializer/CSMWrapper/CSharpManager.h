#pragma once
#include <string>
#include <vector>

using namespace System::Collections::Concurrent;
using namespace System::Reflection;

namespace CSM
{
	ref class CSharpManager
	{
	public:

		static void TryLoadAssembly(std::string& assemblyname);
		static System::Type^ FindType(System::String^% type, System::String^% assemblyname);
		//static void ReloadTypeDefinitions();
		static void Initialize();

	private:
		static ConcurrentDictionary<System::String^, Assembly^>^ m_LoadedAssemblies;
		static ConcurrentDictionary<System::String^, System::Type^>^ m_Types;
	};
}
