#pragma once
#include "../CSMCORE/Header Files/CSMFileProtoType.h"
#include <cliext/vector>
#include <vector>

using namespace System;
using namespace Runtime::InteropServices;

namespace CSM
{
	public ref class FilePrototype : public IDisposable
	{
		delegate CSMSerializedResult* BuildDel(CSMSerializeObj**&, std::int32_t& size);
		CSMFileProtoType * m_BasePrototype;
		static GCHandle m_CallbackHandle;
		static CSMSerializedResult* BuildAction(CSMSerializeObj**& object, std::int32_t& size);

		std::string* m_LangKeyword;
		std::vector<CSMSerializeObj*>* m_MemoryBlocks;

	internal:
		FilePrototype(CSMFileProtoType* base);

	public:
		static void StaticConstructor();
		static void StaticDeconstructor();


		void AddObjects(array<System::Object^>^% objects, int blockindex);
		System::String^ Build();
		~FilePrototype();
	};
}
