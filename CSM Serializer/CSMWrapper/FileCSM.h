#pragma once

#include <string>
#include <functional>
#include <vector>
#include "..\CSMCORE\Header Files\CSMSerializedBlock.h"
#include <map>
#include "CSMDeserializer.h"
#include "..\CSMCORE\Header Files\CSMFileCore.h"

namespace CSM
{
	public ref class FileCSM
	{
	public:

		delegate bool CsharpAddPrototypeDelegate(CSMPrototypeInfo&);

		//Construction and deconstruction
		~FileCSM();
		FileCSM(std::stringstream& stream, bool ignoreassemblies, bool ignoreprototypes, bool ignoreserializedblocks);

		//Loading section
		void LoadAssemblies();
		void MergeAssemblies(String^% newblock);
		void LoadProtoTypes();
		void MergePrototype(String^% newprototype);
		void PrepareSerializedBlocks();
		array<CSMDeserializer^>^ LoadWholeFile();
		void DisposeDeserializerArray(array<CSMDeserializer^>^% deserializers);

		//Getter Section
		System::Int32 GetBlockCount();

		//Deserialization
		CSMDeserializer^ GetDeserializer(Int32 serializedblockindex);

	private:

		//.net type collection
		static System::Collections::Concurrent::ConcurrentDictionary<String^, BuildablePrototype^>^ m_DotNetLoadedTypes;

		//load csharptype callback
		bool LoadCsharpType(CSMPrototypeInfo&);
		static GCHandle m_AddprototypeCallbackHandle;

		//C++ base file
		CSMFileCore * m_BaseFile;
	};
}
