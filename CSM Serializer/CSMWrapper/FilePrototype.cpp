#include "FilePrototype.h"
#include "Marshaling.h"
#include "../CSMCORE/Header Files/FileUtility.h"
#include "../CSMCORE/Header Files/SerializedResult.h"
#include <iterator>

namespace CSM
{
	template class MultiDimentionalArrayLink< CSMSerializedResult>;

	void FilePrototype::StaticConstructor()
	{
		BuildDel^ del = gcnew BuildDel(BuildAction);
		m_CallbackHandle = GCHandle::Alloc(del);
		CSMFileProtoType::CsharpCallback = (CSMFileProtoType::CSharpBuildDelegate)(Marshal::GetFunctionPointerForDelegate(del).ToPointer());
	}

	void FilePrototype::StaticDeconstructor()
	{
		m_CallbackHandle.Free();
	}

	/**
	 * \brief Processes objects marked as being written in C#
	 * \param object 
	 * \param size 
	 * \return 
	 */
	CSMSerializedResult* FilePrototype::BuildAction(CSMSerializeObj**& object, std::int32_t& size)
	{

		array<Object^>^ pointerarr = gcnew array<Object^>(size);

		for (int i = 0; i < size; i++)
		{
			CSMSerializeObj*& obj = object[i];
			if (*obj->m_language == "C#")
			{
				Object^ targetobj = GCHandle::FromIntPtr(IntPtr(obj->m_Objaddress)).Target;
				pointerarr[i] =  targetobj;
			}
		}

		auto outputstring = FileSerialize::SerializeObjects(pointerarr);

		std::string*** cpparr = new std::string**[3];

		for (System::Int32 i = 0; i < outputstring->Length; i++)
		{
			cpparr[i] = new std::string*[static_cast<std::int32_t>(outputstring[i]->Length)];
			for (System::Int32 j = 0; j < outputstring[i]->Length; j++)
			{
				cpparr[i][j] = new std::string[static_cast<std::int32_t>(outputstring[i][j]->Length)];
				for (System::Int32 k = 0; k < outputstring[i][j]->Length; k++)
				{
					cpparr[i][j][k] = CSharpMarshalling::ToStdString(outputstring[i][j][k]);
				}
			}
		}

		//Create the prototypeobjects
		std::int32_t prototypearrsize = static_cast<std::int32_t>(outputstring[1]->Length);
		CSMPrototypeInfo** prototypeinfoarr = new CSMPrototypeInfo*[prototypearrsize];
		for (std::int32_t i = 0; i < prototypearrsize; i++)
		{
			std::int32_t linecount = static_cast<std::int32_t>(outputstring[1][i]->Length);
			prototypeinfoarr[i] = FileUtility::ReadProtoType(cpparr[1][i], linecount);
		}

		//Create SerializedBlocks
		std::int32_t serializedblockarrsize = static_cast<std::int32_t>(outputstring[2]->Length);
		CSMSerializedBlock** blocks = new CSMSerializedBlock*[serializedblockarrsize];
		for (std::int32_t i = 0; i < serializedblockarrsize; i++)
		{
			blocks[i] = FileUtility::ReadSerializedBlock(cpparr[2][i], outputstring[2][i]->Length);
		}


		std::string** assemblies = cpparr[0];

		for (System::Int32 i = 1; i < outputstring->Length; i++)
		{
			for (System::Int32 j = 0; j < outputstring[i]->Length; j++)
			{
				delete[] cpparr[i][j];
			}
			delete[] cpparr[i];
		}

		delete[] cpparr;

		CSMSerializedResult* outputresult = new CSMSerializedResult(assemblies, outputstring[0]->Length, prototypeinfoarr, outputstring[1]->Length, blocks, outputstring[2]->Length);

		return outputresult;
	}

	FilePrototype::FilePrototype(CSMFileProtoType* base)
	{
		m_BasePrototype = base;
		char chars[3] = { 'C', '#' };
		m_LangKeyword = new std::string(chars);
		m_MemoryBlocks = new std::vector<CSMSerializeObj*>();
	}

	FilePrototype::~FilePrototype()
	{
		for (int i = 0; i < m_BasePrototype->obj_amount; i++)
		{
			if (*m_BasePrototype->cached_to_serialize_objects[i]->m_language == "C#")
			{
				CSMSerializeObj*& obj = m_BasePrototype->cached_to_serialize_objects[i];
				GCHandle handle = GCHandle::FromIntPtr(IntPtr(obj->m_Objaddress));
				handle.Free();
			}
		}
		delete m_BasePrototype;
		for (size_t i = 0; i < m_MemoryBlocks->size(); i++)
		{
			CSMSerializeObj*& memoryblock = m_MemoryBlocks->at(i);
			delete[] memoryblock;
		}
		delete m_MemoryBlocks;
		delete m_LangKeyword;
	}

	void FilePrototype::AddObjects(array<Object^>^% objects, int blockindex)
	{
		int size = objects->Length;
		int currentindex = m_MemoryBlocks->size();

		m_MemoryBlocks->emplace_back(new CSMSerializeObj[size]);
		for (int i = 0; i < size; i++)
		{
			GCHandle handle = GCHandle::Alloc(objects[i], GCHandleType::Normal);
			void* ptr = GCHandle::ToIntPtr(handle).ToPointer();
			CSMSerializeObj* obj = new CSMSerializeObj(ptr, m_LangKeyword, blockindex);
			m_MemoryBlocks->at(currentindex)[i] = *obj;
			delete obj;
		}

		m_BasePrototype->AddObjects(m_MemoryBlocks->at(currentindex), size);
	}

	System::String^ FilePrototype::Build()
	{
		//Return the complete prototype file converted into the textfile
		return CSharpMarshalling::ToSystemString(m_BasePrototype->Build());
	}
}
