#include <string>
#include <sstream>
#include "CSMManager.h"
#include "Marshaling.h"
#include "CSharpManager.h"
#include "../CSMCORE/Header Files/FileUtility.h"
#include "CSMDeserializer.h"

namespace CSM
{
	void CSMManager::InitializeCsharpCSM()
	{
		CSharpManager::Initialize();
		CSMDeserializer::StaticConstructor();
		FilePrototype::StaticConstructor();
	}

	void CSMManager::DisposeCsharpCSM()
	{
		CSMDeserializer::StaticDestructor();
		FilePrototype::StaticDeconstructor();
	}

	FileCSM^ CSMManager::OpenFileAtLocation(System::String^% location, bool ignoreassemblies, bool ignoreprototypes, bool ignoreserializedblocks)
	{
		std::string stdlocation(CSharpMarshalling::ToStdString(location));
		std::stringstream filestream = FileUtility::ReadFile(stdlocation);

		return gcnew FileCSM(filestream, ignoreassemblies, ignoreprototypes, ignoreserializedblocks);
	}

	FileCSM^ CSMManager::OpenFile(System::String^% file, bool ignoreassemblies, bool ignoreprototypes, bool ignoreserializedblocks)
	{
		std::stringstream ss;
		ss << CSharpMarshalling::ToStdString(file);
		return gcnew FileCSM(ss, ignoreassemblies, ignoreprototypes, ignoreserializedblocks);
	}

	FilePrototype^ CSMManager::CreateFileTemplate()
	{
		return gcnew FilePrototype(new CSMFileProtoType());
	}
}