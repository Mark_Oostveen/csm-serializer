#include "CSharpManager.h"
#include "Marshaling.h"

using namespace System::Reflection;
using namespace System::Collections;

namespace CSM
{
	void CSharpManager::TryLoadAssembly(std::string& assemblyname)
	{
		System::String^% assemblyreference = CSharpMarshalling::ToSystemString(assemblyname);
		if (m_LoadedAssemblies->ContainsKey(assemblyreference))
			return;

		System::AppDomain^ domain = System::AppDomain::CurrentDomain;
		array<Assembly^>^ assemblies = domain->GetAssemblies();
		for each (Assembly^ element in assemblies)
		{
			if (element->ManifestModule->Name == assemblyreference) {
				m_LoadedAssemblies->TryAdd(assemblyreference, element);
				return;
			}
		}
	}

	System::Type^ CSharpManager::FindType(System::String^% type, System::String^% assemblyname)
	{
		if (m_Types->ContainsKey(type)) {
			System::Type^ cachedoutput;
			m_Types->TryGetValue(type, cachedoutput);
			return cachedoutput;
		}

		Assembly^ assembly;
		m_LoadedAssemblies->TryGetValue(assemblyname, assembly);
		System::Type^ output = assembly->GetType(type);
		m_Types->TryAdd(type, output);
		m_Types->TryGetValue(type, output);

		return output;
	}

	void CSharpManager::Initialize() {
		m_Types = gcnew ConcurrentDictionary<System::String^, System::Type^>();
		m_LoadedAssemblies = gcnew ConcurrentDictionary<System::String^, Assembly^>();
	}

	//void CSharpManager::ReloadTypeDefinitions()
	//{
	//	//load all types of all assemblies
	//	array<System::Reflection::Assembly^>^ assemblies(System::AppDomain::CurrentDomain->GetAssemblies());
	//	Assembly^ calling = Assembly::GetCallingAssembly();

	//	Assembly^ currentassembly = Assembly::GetExecutingAssembly();
	//	for (System::Int32 i = assemblies->Length - 1; i >= 0; i--)
	//	{
	//		if (assemblies[i] == currentassembly) continue;

	//		auto types = assemblies[i]->DefinedTypes->GetEnumerator();

	//		while (types->MoveNext())
	//		{
	//			System::Type^ actualltype = types->Current->UnderlyingSystemType;
	//			System::String^ name = actualltype->FullName;
	//			if (!m_Types.ContainsKey(name))
	//			{
	//				m_Types.TryAdd(actualltype->FullName, actualltype->UnderlyingSystemType);
	//			}
	//		}
	//	}
	//}
}