﻿using System;
using System.Reflection;
using System.Collections.Generic;
using System.Collections.Concurrent;
using System.Runtime.InteropServices;

namespace CSM
{
    public struct BuildablePrototype
    {
        private static ConcurrentDictionary<Type, BuildablePrototype> m_LoadedPrototypes = new ConcurrentDictionary<Type, BuildablePrototype>();

        public Delegate CreateObjectCallback { get; }

        private KeyValuePair<MemberInfo, Delegate>[] m_ObjectFields;

        private Type m_TargetType;

        private BuildablePrototype(ref Type targettype)
        {
            CreateObjectCallback = CSMInstanceFactory.CreateInstanceCallbackByType(ref targettype);

            List<KeyValuePair<MemberInfo, Delegate>> fields = new List<KeyValuePair<MemberInfo, Delegate>>();

            FindFields(fields, targettype);

            m_ObjectFields = fields.ToArray();
            m_TargetType = targettype;

            m_LoadedPrototypes.TryAdd(targettype, this);
        }

        public object Build(ref IntPtr ptr)
        {
            GCHandle handle = GCHandle.FromIntPtr(ptr);
            string[] values = (string[])handle.Target;

            object obj;

            if (m_ObjectFields.Length != 0)
            {
                obj = CreateObjectCallback.DynamicInvoke();
                int fieldinfoindex = 0;
                int valueturn = 0;

                while (fieldinfoindex != m_ObjectFields.Length)
                {
                    BuildObject(m_TargetType, values, obj, ref fieldinfoindex, ref valueturn);
                }
            }
            else
            {
                obj = SetValue(Type.GetTypeCode(m_TargetType), values[0]);
            }

            handle.Free();

            return obj;
        }

        private void BuildObject(in Type parent, in string[] values,in object targetobject, ref int currentfieldinfoindex, ref int valueturn)
        {
            if (currentfieldinfoindex == m_ObjectFields.Length) return;

            Type newtype = CSMObjFactoryShared.FieldType(m_ObjectFields[currentfieldinfoindex].Key);

            MemberInfo member = m_ObjectFields[currentfieldinfoindex].Key;

            TypeCode objtypecode = Type.GetTypeCode(newtype);

            if (!CSMObjFactoryShared.IsPrimitiveType(objtypecode))
            {
                object child = m_ObjectFields[currentfieldinfoindex].Value.DynamicInvoke();
                currentfieldinfoindex++;//TODO NOT BEING TESTED
                BuildObject(newtype, values, child, ref currentfieldinfoindex, ref valueturn);
                return;
            }

            SetValueInMemberInfo(
                member,
                targetobject,
                SetValue(objtypecode, values[valueturn]));

            valueturn++;
            currentfieldinfoindex++;
        }

        private static void FindFields(in List<KeyValuePair<MemberInfo, Delegate>> fields, in Type currenttype)
        {
            if (CSMObjFactoryShared.IsPrimitiveType(Type.GetTypeCode(currenttype))) return;

            MemberInfo[] typefields = CSMObjFactoryShared.GetObjMembers(currenttype);

            //TODO GET ACTUAL TYPE INSTEAD OF DECLARING TYPE
            for (int i = 0; i < typefields.Length; i++)
            {
                Type fieldtypetype = CSMObjFactoryShared.FieldType(typefields[i]);
                fields.Add(
                    new KeyValuePair<MemberInfo, Delegate>(
                        typefields[i],
                        CSMInstanceFactory.CreateInstanceCallbackByType(ref fieldtypetype)));

                FindFields(fields, fieldtypetype);
            }
        }

        private static void SetValueInMemberInfo(in MemberInfo member, in object targetobj, in object value)
        {
            if (member is FieldInfo field)
                field.SetValue(targetobj,value);
            else if (member is PropertyInfo property)
                property.SetValue(targetobj, value);
        }

        private static object SetValue(in TypeCode fieldtype, in string value)
        {
            if (value == "" && fieldtype != TypeCode.String)
            {
                return null;
            }
            if (fieldtype ==  TypeCode.String)
            {
                return value;
            }
            if (fieldtype == TypeCode.Boolean)
            {
                return bool.Parse(value);
            }
            if (fieldtype == TypeCode.Byte)
            {
                return byte.Parse(value);
            }
            if (fieldtype == TypeCode.Char)
            {
                return char.Parse(value);
            }
            if (fieldtype == TypeCode.Int32)
            {
                return int.Parse(value);
            }
            if (fieldtype == TypeCode.Int16)
            {
                return short.Parse(value);
            }
            if (fieldtype == TypeCode.Double)
            {
                return double.Parse(value);
            }
            if (fieldtype == TypeCode.Single)
            {
                return float.Parse(value);
            }
            if (fieldtype == TypeCode.Int64)
            {
                return long.Parse(value);
            }
            if (fieldtype == TypeCode.Decimal)
            {
                return decimal.Parse(value);
            }
            if (fieldtype ==  TypeCode.SByte)
            {
                return sbyte.Parse(value);
            }
            if (fieldtype == TypeCode.UInt32)
            {
                return uint.Parse(value);
            }
            if (fieldtype == TypeCode.UInt16)
            {
                return ushort.Parse(value);
            }

            if (fieldtype != TypeCode.UInt64)
            {
                return ulong.Parse(value);
            }

            return null;
        }

        public static BuildablePrototype GetOrCreate(ref Type targettype)
        {
            if (m_LoadedPrototypes.ContainsKey(targettype))
            {
                m_LoadedPrototypes.TryGetValue(targettype, out BuildablePrototype proto);
                return proto;
            }

            return new BuildablePrototype(ref targettype);
        }
    }
}