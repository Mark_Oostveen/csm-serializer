﻿using System;
using System.Reflection;
using System.Reflection.Emit;

namespace CSM
{
    /// <summary>
    ///     Utility class for rewinding functions
    /// </summary>
    internal static class CSMInstanceFactory
    {
        private delegate T CreatePrototypeCallback<T>();

        public static T CreateObject<T>()
        {
            return Activator.CreateInstance<T>();
        }

        internal static Delegate CreateInstanceCallbackByType(ref Type type)
        {
            if (type != null)
            {
                //check constructor
                ConstructorInfo ctor = null;

                ConstructorInfo[] ctors =
                    type.GetConstructors(BindingFlags.Public | BindingFlags.Instance | BindingFlags.NonPublic);
                for (int i = ctors.Length - 1; i >= 0; i--)
                {
                    if (ctors[i].GetParameters().Length != 0) continue;

                    ctor = ctors[i];
                    break;
                }

                if (ctor == null)
                {
                    Type callbackoutputtype = typeof(CreatePrototypeCallback<>);
                    callbackoutputtype = callbackoutputtype.MakeGenericType(type);

                    MethodInfo createdelegateinfo = typeof(CSMInstanceFactory).GetMethod("CreateObject");
                    createdelegateinfo = createdelegateinfo.MakeGenericMethod(type);

                    return Delegate.CreateDelegate(callbackoutputtype, createdelegateinfo);
                }

                return CilCreateInstanceByType(ref type, ref ctor);
            }

            return null;
        }

        private static Delegate CilCreateInstanceByType(ref Type type, ref ConstructorInfo ctor)
        {
            Type[] parametertypes = new Type[0];

            DynamicMethod method = new DynamicMethod("", type, parametertypes);
            ILGenerator generator = method.GetILGenerator(2);

            generator.Emit(OpCodes.Newobj, ctor);
            generator.Emit(OpCodes.Ret);

            Type delegatetype = typeof(CreatePrototypeCallback<>);
            delegatetype = delegatetype.MakeGenericType(type);

            return method.CreateDelegate(delegatetype);
        }
    }
}