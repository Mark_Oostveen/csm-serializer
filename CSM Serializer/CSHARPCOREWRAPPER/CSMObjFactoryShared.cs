﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Reflection;

namespace CSM
{
    internal static class CSMObjFactoryShared
    {
        internal static MemberInfo[] GetObjMembers(in Type objtype)
        {
            return objtype.GetMembers(BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance).Where(
                x =>
                {
                    MemberTypes type = x.MemberType;
                    if (type == MemberTypes.Field || type == MemberTypes.Event || type == MemberTypes.Property)
                        return true;

                    return false;
                }).ToArray();
        }

        internal static Type FieldType(MemberInfo member)
        {
            MemberTypes type = member.MemberType;

            switch (type)
            {
                case MemberTypes.Field:
                    FieldInfo field = member as FieldInfo;
                    return field.FieldType;

                case MemberTypes.Property:
                    PropertyInfo property = member as PropertyInfo;
                    return property.PropertyType;

                case MemberTypes.Event:
                    EventInfo eventinfo = member as EventInfo;
                    return eventinfo.EventHandlerType;


                default: return null;
            }

        }

        internal static bool IsPrimitiveType(in TypeCode checkingtype)
        {
            switch (checkingtype)
            {
                //TODO implement all types
                case TypeCode.Object:
                    return false;

                default:
                    return true;
            }

            //return (checkingtype == typeof(bool)) || (checkingtype == typeof(byte)) || (checkingtype == typeof(char))
            //       || (checkingtype == typeof(string)) || (checkingtype == typeof(int))
            //       || (checkingtype == typeof(short)) || (checkingtype == typeof(double))
            //       || (checkingtype == typeof(float)) || (checkingtype == typeof(long))
            //       || (checkingtype == typeof(decimal)) || checkingtype.IsEnum || (checkingtype == typeof(sbyte))
            //       || (checkingtype == typeof(uint)) || (checkingtype == typeof(ulong))
            //       || (checkingtype == typeof(ushort));
        }
    }
}
