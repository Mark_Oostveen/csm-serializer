﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using System.Runtime.InteropServices;
using System.Reflection;

namespace CSM
{
    public static class FileSerialize
    {

        private const char SEPARATOR = ',';

        public static unsafe string SerializeObjects(ref void*[] objectpointers)
        {
            //initialize arrays
            GCHandle[] objhandles = new GCHandle[objectpointers.Length];
            object[] objects = new object[objectpointers.Length];

            //extract objects from pointer array
            for (int index = 0; index < objectpointers.Length; index++)
            {
                void* ptr = objectpointers[index];
                objhandles[index] = GCHandle.FromIntPtr(new IntPtr(ptr));
                objects[index] = objhandles[index].Target;
            }

            List<object>[] objectmembers = new List<object>[objects.Length];

            for (int i = 0; i < objectmembers.Length; i++)
            {
                objectmembers[i] = new List<object>();
            }

            //process all object types
            List<Type> extractedtypes = new List<Type>(objectpointers.Length);
            Thread typeworker = new Thread(() =>
            {
                ParallelLoopResult loop = Parallel.For(0, objects.Length,
                    index =>
                    {
                        Type objtype = objects[index].GetType();
                        TryAddMemberToExtractedTypeList(objtype, extractedtypes);
                        ProcessTypes(objects[index], objtype, extractedtypes, objectmembers[index]);
                    });

                while (!loop.IsCompleted)
                {Thread.Sleep(10);}
            });
            typeworker.Start();

            //TODO alloc format strings

            typeworker.Join();

            //clean memory
            for (int i = 0; i < objectpointers.Length; i++)
            {
                objhandles[i].Free();
            }

            return null;
        }

        private static void ProcessTypes(in object obj, in Type objtype, List<Type> typelist, List<object> objmembers)
        {
            MemberInfo[] members = CSMObjFactoryShared.GetObjMembers(objtype);

            for (int i = 0; i < members.Length; i++)
            {
                Type membertype = GetValueFromMember(members[i], obj, typelist, objmembers);
                TryAddMemberToExtractedTypeList(membertype, typelist);
            }
        }

        private static Type GetValueFromMember(in MemberInfo member, in object targetobj, in List<Type> typelist, in List<object> objmembers)
        {

            object value = null;
            Type objtype = null;

            if (member is FieldInfo field)
            {
                if(targetobj != null)
                    value = field.GetValue(targetobj);
                objtype = field.FieldType;
            }
            else if (member is PropertyInfo property)
            {
                if (targetobj != null)
                    value = property.GetValue(targetobj);
                objtype = property.PropertyType;
            }

            if (AddTypeToExtractedTypeList(objtype, value, objmembers)) return objtype;

            ProcessTypes(value, objtype, typelist, objmembers);

            return objtype;
        }

        private static bool AddTypeToExtractedTypeList(in Type mytype, in object value, in List<object> objmembers)
        {
            if (CSMObjFactoryShared.IsPrimitiveType(Type.GetTypeCode(mytype)))
            {
                lock (objmembers)
                    objmembers.Add(value);
                return true;
            }

            return false;
        }

        private static void TryAddMemberToExtractedTypeList(in Type mytype, in List<Type> extractedtypes)
        {
            //extract type references
            if (!extractedtypes.Contains(mytype))
            {
                lock (extractedtypes)
                {
                    extractedtypes.Add(mytype);
                }
            }
        }
    }
}