#include "../Header Files/CSMPrototypeInfo.h"
#include <vector>

namespace CSM
{
	CSMPrototypeInfo::CSMPrototypeInfo(std::int32_t& id, std::int32_t& assemblyId, std::string& language, std::string& mytypename, std::int32_t*& fields, int fieldcount)
	{
		Id = id;
		TypeName = mytypename;
		Language = language;
		m_FieldReferences = fields;
		m_FieldCount = fieldcount;
		m_AssemblyID = assemblyId;
	}
	CSMPrototypeInfo::~CSMPrototypeInfo()
	{
		delete[] m_FieldReferences;
	}
}