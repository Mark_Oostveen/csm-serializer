#pragma once
#include <fstream>
#include <sstream>
#include <vector>
#include <string>
#include "../Header Files/FileUtility.h"

namespace CSM
{
	std::stringstream FileUtility::ReadFile(std::string& filelocation)
	{
		std::ifstream file;
		file.open(filelocation);
		std::stringstream stream;
		stream << file.rdbuf();
		file.close();
		return stream;
	}

	std::vector<std::string>* FileUtility::ReadSectionBlocks(std::string& sectionarr, const char modifier)
	{
		auto arr = new std::vector<std::string>();
		auto line = new std::string();
		arr->reserve(sectionarr.size());

		for (char& i : sectionarr)
		{
			if (i == modifier) {
				if (!line->empty())
				{
					arr->push_back(*line);
					line->clear();
				}

				continue;
			}

			line->push_back(i);
		}

		delete line;

		return arr;
	}

	std::vector<std::string>* FileUtility::ReadSectionBlocksArr(std::string* sectionarr, std::int32_t& size, const char modifier)
	{
		auto arr = new std::vector<std::string>();

		for (std::int32_t i = 0; i < size; i++)
		{
			std::string& linestring = sectionarr[i];
			arr->emplace_back(linestring);
		}

		return arr;
	}

	CSMPrototypeInfo* FileUtility::ReadProtoType(std::string& prototypeblock)
	{
		auto arr = ReadSectionBlocks(prototypeblock, '\n');
		std::int32_t id = std::stoi(arr->at(0));
		std::int32_t assemblyid = std::stoi(arr->at(1));
		std::string typeName = arr->at(2);
		std::string language = arr->at(3);

		std::int32_t* fields = new std::int32_t[arr->size() - 4];
		for (uint32_t i = 4; i < arr->size(); i++)
		{
			fields[i - 4] = std::stoi(arr->at(i));
		}

		delete arr;

		return new CSMPrototypeInfo(id, assemblyid, language, typeName, fields, arr->size() - 4);
	}

	CSMPrototypeInfo* FileUtility::ReadProtoType(std::string* prototypeblockarray, std::int32_t& size)
	{
		auto arr = ReadSectionBlocksArr(prototypeblockarray, size, '\n');
		std::int32_t id = std::stoi(arr->at(0));
		std::int32_t assemblyid = std::stoi(arr->at(1));
		std::string typeName = arr->at(2);
		std::string language = arr->at(3);

		std::int32_t* fields = new std::int32_t[size - 4];

		for (int i = 4; i < size; i++)
		{
			fields[i - 4] = std::stoi(arr->at(i));
		}

		delete arr;

		return new CSMPrototypeInfo(id, assemblyid, language, typeName, fields, arr->size() - 4);
	}

	CSMSerializedBlock* FileUtility::ReadSerializedBlock(std::string& prototypeblockarr)
	{
		auto myarr = ReadSectionBlocks(prototypeblockarr, '\n');
		if (myarr->size() == 0) return nullptr;
		std::int32_t id = std::stoi(myarr->at(0));
		std::int32_t count = std::stoi(myarr->at(1));
		std::int32_t prototypeid = std::stoi(myarr->at(2));

		std::int32_t size = myarr->size();
		auto container = new std::string[size - 3];

		for (int i = 3; i < size; i++)
		{
			container[i - 3] = myarr->at(i);
		}

		delete myarr;

		return new CSMSerializedBlock(id, count, prototypeid, container, size - 3);
	}

	CSMSerializedBlock* FileUtility::ReadSerializedBlock(std::string*& prototypeblock, std::int32_t size)
	{
		std::int32_t id, count, prototypeid;
		id = std::stoi(prototypeblock[0]);
		count = std::stoi(prototypeblock[1]);
		prototypeid = std::stoi(prototypeblock[2]);

		auto container = new std::string[size - 3];

		for (std::int32_t i = 3; i < size; i++)
		{
			container[i - 3] = prototypeblock[i];
		}

		return new CSMSerializedBlock(id, count, prototypeid, container, size - 3);;
	}
}