#include "../Header Files/CSMDeserializerCore.h"

namespace CSM
{
	CSMDeserializerCore::CppBuildDelegate CSMDeserializerCore::CppCallback = nullptr;
	CSMDeserializerCore::CSharpBuildDelegate CSMDeserializerCore::CsharpCallback = nullptr;

	CSMDeserializerCore::CSMDeserializerCore(CSMSerializedBlock& block, CSMPrototypeInfo& prototype, std::string& assemblyname)
	{
		m_Block = &block;
		m_Prototype = &prototype;
		Assembly = &assemblyname;
	}

	CSMDeserializerCore::~CSMDeserializerCore()
	{
		m_Block = nullptr;
		m_Prototype = nullptr;
		Assembly = nullptr;
	}

	void* CSMDeserializerCore::Build()
	{
		if (m_Prototype->Language == "C#")
		{
			return CsharpCallback(*this);
		}

		return CppCallback(*this);
	}

	CSMSerializedBlock * CSMDeserializerCore::GetBlockPTR() const
	{
		return m_Block;
	}

	CSMPrototypeInfo * CSMDeserializerCore::GetPrototypePTR() const
	{
		return m_Prototype;
	}
}