#include "../Header Files/CSMFileProtoType.h"
#include <thread>
#include <condition_variable>

namespace CSM {
	CSMFileProtoType::CppBuildDelegate CSMFileProtoType::CppCallback = nullptr;
	CSMFileProtoType::CSharpBuildDelegate CSMFileProtoType::CsharpCallback = nullptr;

	std::string CSMFileProtoType::ProcessCallbacks(CSMSerializeObj**& objectarr, std::int32_t& size)
	{
		//results
		auto results = new MultiDimentionalArrayLink<CSMSerializedResult>(1);

		//
		//TODO sort input objects into their blocks and loop through block creation
		//SortObjectsToBlocks();
		//

		std::thread csharpthread([&]
		{
			//TODO process data
			CSMSerializedResult* result = CsharpCallback(objectarr, size);
			results->ReplaceItem(result, 0);
		});


		//For Later C++ implementation

		//std::thread cppthread([&]
		//{
		//	cpplock.lock();
		//	//TODO process data
		//	results.push_back(std::shared_ptr<CSMSerializedResult>(CppCallback(objects, size)));
		//	cpplock.unlock();
		//	cppwait.notify_all();
		//});


		csharpthread.join();

		//Order prototypes
		std::string outputstring = ProcessAssemblies(*results);

		outputstring.append("*\n");
		outputstring.append(ProcessCallbackResultsSerializedBlocks(*results));
		delete results;
		return outputstring;
	}

	void CSMFileProtoType::SortObjectsToBlocks(CSMSerializeObj **&)
	{
		//TODO sort array contents into blocks
	}

	std::string CSMFileProtoType::ProcessAssemblies(MultiDimentionalArrayLink<CSMSerializedResult>& results)
	{
		std::string assemblyblock = "";
		for (std::int32_t i = 0; i < results.m_ArraySize; i++)
		{
			for (std::int32_t j = 0; j < 2; j++)
			{
				std::int32_t& fieldcount = results[i]->m_AssemblyCount;
				for(std::int32_t k = 0; k < fieldcount; k++)
				{
					std::string& field = results[i]->m_Assemblies[j][k];
					assemblyblock.append(field);
					if (k + 1 == fieldcount) continue;
					assemblyblock.append(",");
				}
				assemblyblock.append("\n");
			}
		}
		return assemblyblock;
	}

	std::string CSMFileProtoType::ProcessCallbackResultsSerializedBlocks(MultiDimentionalArrayLink<CSMSerializedResult>& results)
	{
		int offset = 0;
		std::string prototypeblock = "";
		std::string serializedblock = "";
		for (std::int32_t i = 0; i < results.m_ArraySize; i++)
		{
			for (int j = 0; j < results.m_ArrayPointer[i]->m_PrototypeCount; j++)
			{
				CSMPrototypeInfo*& proto = results.m_ArrayPointer[i]->m_PrototypesArray[j];
				proto->Id += offset;
				prototypeblock.append(std::to_string(proto->Id));
				prototypeblock.append("\n");
				prototypeblock.append(std::to_string(proto->m_AssemblyID));
				prototypeblock.append("\n");
				prototypeblock.append(proto->TypeName);
				prototypeblock.append("\n");
				prototypeblock.append(proto->Language);
				prototypeblock.append("\n");
				for (int k = 0; k < proto->m_FieldCount; k++)
				{
					prototypeblock.append(std::to_string(proto->m_FieldReferences[k]));
					prototypeblock.append("\n");
				}
				prototypeblock.append("?\n");
			}

			for (int j = 0; j < results.m_ArrayPointer[i]->m_SerializedBlockCount; j++)
			{
				CSMSerializedBlock*& block = results.m_ArrayPointer[i]->m_BlocksArray[j];
				block->PrototypeID += offset;
				serializedblock.append(std::to_string(block->ID));
				serializedblock.append("\n");
				serializedblock.append(std::to_string(block->Count));
				serializedblock.append("\n");
				serializedblock.append(std::to_string(block->PrototypeID));
				serializedblock.append("\n");
				for (int k = 0; k < block->DataSize; k++)
				{
					serializedblock.append(block->Data[k]);
					serializedblock.append("\n");
				}
				serializedblock.append("?\n");
			}

			offset += results.m_ArrayPointer[i]->m_PrototypeCount;
		}

		prototypeblock.append("*\n");
		serializedblock.append("*");
		return prototypeblock.append(serializedblock);
	}

	CSMFileProtoType::~CSMFileProtoType()
	{
		delete[] cached_to_serialize_objects;
	}

	void CSMFileProtoType::AddObjects(CSMSerializeObj *& objarr, int arrsize)
	{
		CSMSerializeObj** newarr = new CSMSerializeObj*[arrsize + obj_amount];
		for (int i = 0; i < obj_amount; i++)
		{
			newarr[i] = cached_to_serialize_objects[i];
		}

		delete[] cached_to_serialize_objects;
		int index = 0;
		for (int i = obj_amount; i < obj_amount + arrsize; i++ && index++)
		{
			newarr[i] = &objarr[index];
		}

		obj_amount += arrsize;
		cached_to_serialize_objects = newarr;
	}

	std::string CSMFileProtoType::Build()
	{

		if (obj_amount == 0) return "";

		std::string output = ProcessCallbacks(cached_to_serialize_objects, obj_amount);

		return output;
	}
}