#include <string>
#include "../Header Files/CSMSerializedBlock.h"
#include "../Header Files/FileUtility.h"

namespace CSM
{
	CSMSerializedBlock::CSMSerializedBlock(std::int32_t& id, std::int32_t& count, std::int32_t& prototypeid, std::string*& data, std::int32_t datasize)
	{
		ID = id;
		Count = count;
		PrototypeID = prototypeid;
		Data = data;
		DataSize = datasize;
	}

	CSMSerializedBlock::~CSMSerializedBlock()
	{
		delete[] Data;
	}
}
