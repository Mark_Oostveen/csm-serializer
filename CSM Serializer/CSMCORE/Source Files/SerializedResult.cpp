#include "../Header Files/SerializedResult.h"

namespace CSM
{
	CSMSerializedResult::CSMSerializedResult(std::string**& assemblies, int assemblycount, CSMPrototypeInfo**& prototypes, int prototypecount, CSMSerializedBlock**& serializedblocks, int blockcount)
	{
		m_Assemblies = assemblies;
		m_AssemblyCount = assemblycount;
		m_BlocksArray = serializedblocks;
		m_SerializedBlockCount = blockcount;
		m_PrototypesArray = prototypes;
		m_PrototypeCount = prototypecount;
	}

	CSMSerializedResult::~CSMSerializedResult()
	{
		for (int i = 0; i < m_PrototypeCount; i++)
		{
			delete m_PrototypesArray[i];
		}

		delete[] m_PrototypesArray;

		for (int i = 0; i < m_SerializedBlockCount; i++)
		{
			delete m_BlocksArray[i];
		}

		delete[] m_BlocksArray;

		for (int i = 0; i < m_AssemblyCount; i++)
		{
			delete[] m_Assemblies[i];
		}

		delete[] m_Assemblies;
	}
}