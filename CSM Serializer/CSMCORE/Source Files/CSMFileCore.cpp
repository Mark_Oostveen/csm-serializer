#include <sstream>
#include <bitset>
#include "../Header Files/CSMFileCore.h"
#include "../Header Files/CSMDeserializerCore.h"
#include "../Header Files/FileUtility.h"
#include "..\Header Files\CSMSerializedBlock.h"

namespace CSM
{
	std::int32_t CSMFileCore::GetSerializedBlockCount() const
	{
		return m_SerializedBlockList->size();
	}

	std::string& CSMFileCore::GetAssemblies() const
	{
		return *m_Assemblies;
	}

	CSMFileCore::CSMFileCore(std::stringstream& file, bool ignoreassemblies, bool ignoreprototypes, bool ignoreserializedBlocks, CSharpCreatePrototype callback)
	{
		CSharpAddPrototype = callback;
		std::bitset<2> section;
		std::string line;

		m_ProtoTypes = new std::string();
		m_Assemblies = new std::string();
		m_SerializedBlocks = new std::string();
		LoadedAssemblies = new std::vector<std::string>();
		m_SerializedBlockList = new std::vector<CSMSerializedBlock*>();
		m_PrototypeReference = new std::map <std::int32_t, CSMPrototypeInfo*>();


		while (std::getline(file, line, '*'))
		{
			if (!section[0] && !section[1])
			{
				section = section.set(0, true);
				if (ignoreassemblies) continue;
				m_Assemblies->append(line);
				continue;
			}
			if (section[0] && !section[1])
			{
				section = section.set(1, true);

				if (ignoreprototypes) continue;
				m_ProtoTypes->append(line);
				continue;
			}
			if (section[0] && section[1])
			{
				if (ignoreserializedBlocks) continue;
				m_SerializedBlocks->append(line);
			}
		}
	}

	CSMFileCore::~CSMFileCore()
	{
		delete m_Assemblies;
		delete m_ProtoTypes;
		delete m_SerializedBlocks;

		for (size_t i = 0; i < m_SerializedBlockList->size(); i++)
		{
			delete m_SerializedBlockList->at(i);
		}

		delete m_SerializedBlockList;

		for (auto& iterator : *m_PrototypeReference)
		{
			delete iterator.second;
		}

		delete m_PrototypeReference;
		delete LoadedAssemblies;
	}

	void CSMFileCore::PrepareSerializedBlocks() const
	{
		auto serializedsections = FileUtility::ReadSectionBlocks(*m_SerializedBlocks, '?');

		m_SerializedBlockList->reserve(serializedsections->size());
		for (std::string& serializedsection : *serializedsections)
		{
			//make ptr
			m_SerializedBlockList->emplace_back(FileUtility::ReadSerializedBlock(serializedsection));
		}

		delete serializedsections;
		*m_SerializedBlocks = "";
	}

	void CSMFileCore::MergeSerializedBlock(std::string & newserializedblock) const
	{
		m_SerializedBlocks->append(newserializedblock);
		PrepareSerializedBlocks();
	}

	void CSMFileCore::LoadProtoTypes() const
	{
		auto arr = FileUtility::ReadSectionBlocks(*m_ProtoTypes, '?');

		//add matched cases into an heap allocated vector
		std::int32_t currentiteration = 0;
		for (std::string& it : *arr)
		{
			CSMPrototypeInfo* instance = FileUtility::ReadProtoType(it);
			//TODO Implement C++ Way
			if (instance->Language == "C#")
			{
				if (CSharpAddPrototype(*instance))
				{
					m_PrototypeReference->insert(std::pair<std::int32_t, CSMPrototypeInfo*>(currentiteration, instance));
					currentiteration++;
				}
			}
		}

		delete arr;
		*m_ProtoTypes = "";
	}
	void CSMFileCore::MergePrototype(std::string & newprototype) const
	{
		m_ProtoTypes->append(newprototype);
		LoadProtoTypes();
	}

	CSMDeserializerCore* CSMFileCore::GetDeserializer(const std::int32_t serializedblockindex, std::string& assemblyname) const
	{
		CSMSerializedBlock* block = m_SerializedBlockList->at(serializedblockindex);
		CSMPrototypeInfo& prototype = *m_PrototypeReference->at(block->PrototypeID);
		return new CSMDeserializerCore(*block, prototype, assemblyname);
	}
}