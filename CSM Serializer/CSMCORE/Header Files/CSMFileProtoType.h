#pragma once
#include <string>
#include <vector>
#include "Serializeobj.h"
#include "SerializedResult.h"
#include "MultiDimentionalArrayLink.h"

namespace CSM {

	class CSMSerializedResult;

	class CSMFileProtoType
	{

		static std::string ProcessCallbacks(CSMSerializeObj**& objectarr, std::int32_t& size);
		static std::string ProcessAssemblies(MultiDimentionalArrayLink<CSMSerializedResult>& results);
		static std::string ProcessCallbackResultsSerializedBlocks(MultiDimentionalArrayLink<CSMSerializedResult>& results);

	public:

		typedef CSMSerializedResult* (__stdcall *CSharpBuildDelegate)(CSMSerializeObj**& objectarr, std::int32_t&);
		typedef CSMSerializedResult* (__cdecl *CppBuildDelegate)(CSMSerializeObj**& objectarr, std::int32_t&);

		static CSharpBuildDelegate CsharpCallback;
		//TODO cpp needs to be set when implementing C++ way
		static CppBuildDelegate CppCallback;

		CSMSerializeObj** cached_to_serialize_objects;
		int obj_amount;

		CSMFileProtoType() = default;
		~CSMFileProtoType();

		void AddObjects(CSMSerializeObj*&, int);
		void SortObjectsToBlocks(CSMSerializeObj**&);

		std::string Build();
	};
}
