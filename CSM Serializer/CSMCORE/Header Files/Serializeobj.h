#pragma once
#include <string>

namespace CSM
{
	class CSMSerializeObj
	{
	public:
		void* m_Objaddress;
		std::string* m_language;
		int m_BlockIndex;
		CSMSerializeObj() = default;
		CSMSerializeObj(void* objadress, std::string* language, int blockindex);
		virtual ~CSMSerializeObj() {};
	};
}
