#pragma once
#include <cstdint>

namespace CSM
{
	template<class T>
	class MultiDimentionalArrayLink
	{
	public:
		T** m_ArrayPointer;
		std::int32_t m_ArraySize;

		MultiDimentionalArrayLink(std::int32_t size);
		~MultiDimentionalArrayLink();

		T*& operator[] (std::int32_t& index);
		void ReplaceItem(T* newitem, int index);
	};


	template<class T >
	MultiDimentionalArrayLink<T>::MultiDimentionalArrayLink(std::int32_t size)
	{

		m_ArrayPointer = new T*[size];
		m_ArraySize = size;
	}
	template<class T >
	MultiDimentionalArrayLink<T>::~MultiDimentionalArrayLink()
	{
		for (int32_t i = 0; i < m_ArraySize; i++)
		{
			delete m_ArrayPointer[i];
		}
		delete[] m_ArrayPointer;
	}
	template<class T >
	T*& MultiDimentionalArrayLink<T>::operator[](std::int32_t& index)
	{
		return m_ArrayPointer[index];
	}
	template<class T >
	void MultiDimentionalArrayLink<T>::ReplaceItem(T* newitem, int index)
	{
		m_ArrayPointer[index] = newitem;
	}
}
