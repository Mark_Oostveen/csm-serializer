#pragma once
#include "CSMFileProtoType.h"
#include "CSMPrototypeInfo.h"
#include "CSMSerializedBlock.h"

namespace CSM
{
	class CSMSerializedResult
	{
		friend class CSMFileProtoType;

		CSMSerializedResult() = default;
		std::string** m_Assemblies;
		int m_AssemblyCount;
		CSMPrototypeInfo** m_PrototypesArray;
		int m_PrototypeCount;
		CSMSerializedBlock** m_BlocksArray;
		int m_SerializedBlockCount;

	public:
		CSMSerializedResult(std::string**& assemblies, int assemblycount, CSMPrototypeInfo**& prototypes, int prototypecount, CSMSerializedBlock**& serializedblocks, int blockcount);
		virtual ~CSMSerializedResult();

	};
}
