#pragma once
#include <iosfwd>
#include <string>
#include <vector>
#include "CSMPrototypeInfo.h"
#include "CSMSerializedBlock.h"


namespace CSM
{
	class FileUtility
	{
	public:
		static std::stringstream ReadFile(std::string& filelocation);
		static std::vector<std::string>* ReadSectionBlocks(std::string& section, const char modifier);
		static std::vector<std::string>* ReadSectionBlocksArr(std::string* sectionarr, std::int32_t& size, const char modifier);
		static CSMPrototypeInfo* ReadProtoType(std::string& prototypeblock);
		static CSMPrototypeInfo* ReadProtoType(std::string* prototypeblockarray, std::int32_t& size);
		static CSMSerializedBlock* ReadSerializedBlock(std::string& prototypeblockarr);
		static CSMSerializedBlock* ReadSerializedBlock(std::string*& prototypeblock, std::int32_t size);
	};
}
