#pragma once
#include <string>
#include <map>
#include "CSMSerializedBlock.h"
#include "CSMPrototypeInfo.h"
#include "CSMDeserializerCore.h"

namespace CSM
{
	class CSMFileCore
	{
	public:
		typedef bool(__stdcall *CSharpCreatePrototype)(CSMPrototypeInfo&);

		//statics
		CSharpCreatePrototype CSharpAddPrototype;

		//getters
		std::int32_t GetSerializedBlockCount() const;
		std::string& GetAssemblies() const;
		std::vector<std::string>* LoadedAssemblies;

		CSMFileCore(std::stringstream& file, bool ignoreassemblies, bool ignoreprototypes, bool ignoreserializedBlocks, CSharpCreatePrototype callback);
		~CSMFileCore();

		void PrepareSerializedBlocks() const;
		void MergeSerializedBlock(std::string& newserializedblock) const;
		void LoadProtoTypes() const;
		void MergePrototype(std::string& newprototype) const;

		CSMDeserializerCore* GetDeserializer(std::int32_t serializedblockindex, std::string& assemblyname) const;

	private:
		//nonstatic
		std::vector<CSMSerializedBlock*>* m_SerializedBlockList;
		std::map <std::int32_t, CSMPrototypeInfo*>* m_PrototypeReference;

		//variables
		std::string* m_Assemblies;
		std::string* m_ProtoTypes;
		std::string* m_SerializedBlocks;
	};
}
