#pragma once
#include <vector>
#include <string>

namespace CSM
{
	class FileUtility;

	struct CSMSerializedBlock
	{
		friend class FileUtility;

		std::int32_t ID{};
		std::int32_t Count{};
		std::int32_t PrototypeID{};
		std::string* Data{};
		std::int32_t DataSize;

		CSMSerializedBlock() = default;
		~CSMSerializedBlock();
	private:
		CSMSerializedBlock(std::int32_t& id, std::int32_t& count, std::int32_t& prototypeid, std::string*& data, std::int32_t datasize);
	};
}
