#pragma once
#include "CSMPrototypeInfo.h"
#include "CSMSerializedBlock.h"

namespace CSM
{
	class CSMDeserializerCore
	{
	public:

		typedef void* (__stdcall *CSharpBuildDelegate)(const CSMDeserializerCore&);
		typedef void* (__cdecl *CppBuildDelegate)(const CSMDeserializerCore&);

		static CSharpBuildDelegate CsharpCallback;
		//TODO cpp needs to be set when implementing C++ way
		static CppBuildDelegate CppCallback;

		CSMDeserializerCore(CSMSerializedBlock& block, CSMPrototypeInfo& prototype, std::string& assemblyname);
		~CSMDeserializerCore();

		//C++Building
		void* Build();

		CSMSerializedBlock* GetBlockPTR() const;
		CSMPrototypeInfo* GetPrototypePTR() const;
		std::string* Assembly;

	private:
		CSMSerializedBlock * m_Block;
		CSMPrototypeInfo* m_Prototype;
	};
}
