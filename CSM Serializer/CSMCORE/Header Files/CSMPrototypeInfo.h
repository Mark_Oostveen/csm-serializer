#pragma once
#include <string>
#include <vector>

namespace CSM
{
	struct CSMPrototypeInfo
	{

		std::int32_t Id;
		std::int32_t m_AssemblyID;
		std::string TypeName;
		std::string Language;
		std::int32_t* m_FieldReferences;
		std::int32_t m_FieldCount;

		CSMPrototypeInfo() = default;
		CSMPrototypeInfo(std::int32_t& id, std::int32_t& assemblyId, std::string& language, std::string& mytypename, std::int32_t*& fields, int fieldcount);
		~CSMPrototypeInfo();
	};
}
