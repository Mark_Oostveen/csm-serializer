﻿using System;
using System.Diagnostics;
using CSM;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace CSMCSHARPCore
{
    //public class vector0
    //{
    //    public vector0()
    //    {
    //        x = 99;
    //    }

    //    internal float x;
    //}
    //public class vector1
    //{
    //    public vector1()
    //    {
    //        x = 99;
    //    }

    //    internal float x;
    //}
    //public class Vector2
    //{
    //    protected Vector2()
    //    {
    //        x = 0;
    //        y = 0;
    //        m_Vector = new vector1();
    //    }

    //    internal vector1 m_Vector;

    //    internal float x, y;
    //}

    //public class Vector22 : Vector2
    //{
    //    protected internal Vector22()
    //    {

    //    }
    //}

    public class TestClass
    {
        protected internal float x;

        protected internal TestClass() { }

    }

    internal class Program
    {
        private const string m_FilePath = "P:\\Projects\\Development--P L U G I N S\\CSM-Serializer\\---TEST_FILES---\\NewTest.csm";
        private const string m_CreateFilePath = "P:\\Projects\\Development--P L U G I N S\\CSM-Serializer\\---TEST_FILES---\\Testcreations";

        private static void Main(string[] args)
        {
            //Load CSMbackend compatibility for .Net types
            CSMManager.InitializeCsharpCSM();

            //TEST
            //Parallel.For(0, 50, index => CreatePrototypeFile(m_CreateFilePath));

            Console.WriteLine("CSM Loaded");

            long time = 0;

            //Opening file Test
            //for (int i = 0; i < 3; i++)
            //{
            //    time += OpenFile(m_FilePath);
            //}

            //Console.WriteLine("Overal Opening Time: " + time + "ms");

            time = 0;

            //Creating File Test
            for (int i = 0; i < 25; i++)
            {
                time += CreatePrototypeFile(m_CreateFilePath);
            }

            Console.WriteLine("Overal Creating Time: " + time + "ms");

            //Unload .net Type Backend
            CSMManager.DisposeCsharpCSM();

            Console.WriteLine("CSM Unloaded");
        }

        private static long OpenFile(string filelocation)
        {
            Stopwatch sw = new Stopwatch();
            sw.Start();

            long openfile, loadassemblies, loadprototypes, prepare, deserialize = 0;

            //Open File & load all parts of file
            FileCSM file = CSMManager.OpenFileAtLocation(ref filelocation, false, false, false);
            sw.Stop();
            openfile = sw.ElapsedMilliseconds;
            sw.Restart();
            file.LoadAssemblies();
            loadassemblies = sw.ElapsedMilliseconds;
            sw.Restart();
            file.LoadProtoTypes();
            loadprototypes = sw.ElapsedMilliseconds;
            sw.Restart();
            file.PrepareSerializedBlocks();
            prepare = sw.ElapsedMilliseconds;

            //Loop throught all assemblies and deserialize
            for (int j = 0; j < file.GetBlockCount(); j++)
            {
                sw.Restart();
                CSMDeserializer deserializer = file.GetDeserializer(j);
                //BuildCSharp is a shortcut function, this will already interpret void pointer data this will eliminate need for unsafe context
                object[] output = deserializer.Build();

                deserialize += sw.ElapsedMilliseconds;

                //Dispose the deserializer
                deserializer.Dispose();
            }

            sw.Restart();

            sw.Stop();

            //dispose the file
            file.Dispose();

            //list run results
            Console.WriteLine("Run Details:");
            Console.WriteLine("File: " + filelocation);
            Console.WriteLine("OpenFile ms: " + openfile + "ms");
            Console.WriteLine("Loading Assemblies: " + loadassemblies + "ms");
            Console.WriteLine("Loading Prototypes: " + loadprototypes + "ms");
            Console.WriteLine("Preparing Serialized blocks: " + prepare + "ms");
            Console.WriteLine("De serializing(instantiating objects): " + deserialize + "ms");

            long totaltime = openfile + loadassemblies + loadprototypes + prepare + deserialize;

            Console.WriteLine("Total Time: " + totaltime + "ms \n \n \n");

            return totaltime;
        }

        private static long CreatePrototypeFile(string location)
        {
            Stopwatch sw = new Stopwatch();


            List<object> TESTITEMS = new List<object>();

            Random random1 = new Random();

            for (int i = 0; i < 5000; i++)
            {
                TESTITEMS.Add(new TestClass(){ x = 8 });
            }

            object[] objects = TESTITEMS.ToArray();

            sw.Start();

            FilePrototype prototype = CSMManager.CreateFileTemplate();

            prototype.AddObjects(ref objects);

            string output = prototype.Build();

            prototype.Dispose();

            sw.Stop();
            Console.WriteLine("Serialize Delay " + sw.ElapsedMilliseconds);
            sw.Reset();
            sw.Start();

            FileCSM file = CSMManager.OpenFile(ref output, false, false, false);
            CSMDeserializer[] deserializers = file.LoadWholeFile();
            for (int i = 0; i < deserializers.Length; i++)
            {
                object[] objs = deserializers[i].Build();
            }
            file.DisposeDeserializerArray(ref deserializers);
            file.Dispose();

            sw.Stop();

            //TODO function here

            Console.WriteLine("Deserialize Delay " + sw.ElapsedMilliseconds);

            return sw.ElapsedMilliseconds;
        }
    }
}