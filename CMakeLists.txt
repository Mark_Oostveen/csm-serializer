cmake_minimum_required(VERSION 3.10)
project(CSM_Serializer)

set(CMAKE_CXX_STANDARD 11)

include_directories("CSM Serializer")
include_directories("CSM Serializer/CSM++Core")
include_directories("CSM Serializer/CSM++Core/Debug")
include_directories("CSM Serializer/CSM++Core/Source")
include_directories("CSM Serializer/CSM++Core/Source/Cpp")
include_directories("CSM Serializer/CSM++Core/Source/Header")
include_directories("CSM Serializer/CSM++Core/Source/Header/Normal headers")
include_directories("CSM Serializer/CSM++Core/Source/Header/Wrapper")

add_executable(CSM_Serializer
        "CSM Serializer/CSM++Core/Debug/CSM++Core.pch"
        "CSM Serializer/CSM++Core/Source/Cpp/Normal CPP/CSMFile.cpp"
        "CSM Serializer/CSM++Core/Source/Cpp/Wrapper/CppWrapper.cpp"
        "CSM Serializer/CSM++Core/Source/Cpp/Wrapper/FileWrapper.cpp"
        "CSM Serializer/CSM++Core/Source/Cpp/Wrapper/Marshaling.cpp"
        "CSM Serializer/CSM++Core/Source/Header/Normal headers/CSMFile.h"
        "CSM Serializer/CSM++Core/Source/Header/Wrapper/CppWrapper.h"
        "CSM Serializer/CSM++Core/Source/Header/Wrapper/FileWrapper.h"
        "CSM Serializer/CSM++Core/Source/Header/Wrapper/Marshaling.h"
        "CSM Serializer/CSM++Core/AssemblyInfo.cpp"
        "CSM Serializer/CSM++Core/Resource.h"
        "CSM Serializer/CSM++Core/stdafx.cpp"
        "CSM Serializer/CSM++Core/stdafx.h")
